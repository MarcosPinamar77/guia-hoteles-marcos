$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    })
});
document.getElementById('sendBtn').addEventListener('click', ()=>{
    $('#exampleModal').modal('hide')
})


$('#exampleModal').on('show.bs.modal', function(e){
    console.log("Abriendo modal");
    $('#openModal').removeClass('btn-outline-info');
    $('#openModal').addClass('btn-danger');
    $('#openModal').prop('disabled', true);
});
$('#exampleModal').on('shown.bs.modal', function(e){
    console.log("El modal se abió");
});
$('#exampleModal').on('hide.bs.modal', function(e){
    console.log("Cerrando modal");
    $('#openModal').removeClass('btn-danger');
    $('#openModal').addClass('btn-outline-info');
    $('#openModal').prop('disabled', false);
});
$('#exampleModal').on('hidden.bs.modal', function(e){
    console.log("El modal se cerró");
});